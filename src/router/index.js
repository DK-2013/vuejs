import Vue from 'vue'
import Router from 'vue-router'
import VGrid from '@/components/VGrid'
import VFormEdit from '@/components/VFormEdit'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', redirect: '/list/' },
    {
      path: '/list*',
      name: 'list',
      component: VGrid,
      props: (route) => ({page: route.query.page, limit: route.query.limit})
    },
    {
      path: '/user/add',
      component: VFormEdit
    },
    {
      path: '/user/:id',
      component: VFormEdit,
      props: true
    }
  ]
})
